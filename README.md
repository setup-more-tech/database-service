# DatabaseService

Данный сервис отвечает за коммуникацию с базой данных MySQL (развернута на ```localhost:3306```) и отвечает за CRUD (Create, Read, Update, Delete) с таблицами о **метриках используемости** данных, **ссылки на финальные датасеты** (не реализовано) и **фичи каждого датасета**.

## Установка

Чтобы запустить данный контейнер необходимо запустить команду

    docker run -d -p 8086:5000 registry.gitlab.com/setup-more-tech/database-service
