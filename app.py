from flask import Flask, render_template, json
from flaskext.mysql import MySQL

mysql = MySQL()
app = Flask(__name__)
app.config.from_pyfile('database.cfg')
app.config.from_pyfile('/etc/dbhost.cfg')

mysql.init_app(app)

@app.route('/getFeatures')
def getFeatures():
    try:
        # connect to mysql
        connection = mysql.connect()
        cursor = connection.cursor()
        cursor.execute('''SELECT * FROM FEEATURES''')
        result = cursor.fetchall()
	    
        students = []
        for entry in result:
            record = {
                    'id': entry[0],
                    'dataset': entry[1],
                    'feature': entry[2],
                    'value': entry[3]
            }
            students.append(record)		

        return record
    except Exception as e:
        return json.dumps({'error':str(e)})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
        cursor.close()
        connection.close()

@app.route('/getUsage')
def getUsage():
    try:
        # connect to mysql
        connection = mysql.connect()
        cursor = connection.cursor()
        cursor.execute('''SELECT * FROM USAGE''')
        result = cursor.fetchall()
	    
        students = []
        for entry in result:
            record = {
                    'id': entry[0],
                    'name': entry[1],
                    'value': entry[2]
            }
            students.append(record)		

        return record
    except Exception as e:
        return json.dumps({'error':str(e)})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
        cursor.close()
        connection.close()

if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True, port=5000)